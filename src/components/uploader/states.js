export default {
  WAITING: '等待中',
  UPLOADING: '上传中',
  COMPLETE: '上传完成',
  CANCELED: '取消上传',
  FAILED: '上传失败',
}
