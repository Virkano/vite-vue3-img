import { createApp, nextTick } from 'vue'
import App from './App.vue'
import icons from './components/icons/allIcons'
import { cl } from './util/utils'
import Model from './components/Model.vue'
import Toast from './components/toast/Toast.vue'

const app = createApp(App)

for (const [key, value] of Object.entries(icons)) {
  app.component(key, value)
}

app.component('app-model', Model)
app.component('app-toast', Toast)

app.config.globalProperties.$cl = cl

//全局指令 v-focus
app.directive('focus', {
  mounted(el) {
    nextTick(() => {
      const selectionEnd = el.value.split('.').slice(0, -1).join('.').length
      el.setSelectionRange(0, selectionEnd)
      el.focus()
    })
  },
})

app.mount('#app')
