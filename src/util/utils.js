/**
 * @description:  console.log() 🥱 增添乐趣 🎉🎉🎉
 */
export function cl(str, color) {
  return console.log(`%c${str}`, `color: ${color}`)
}
